package com.example.demo.service;

import com.example.demo.model.StatsBean;

/**
 * Interface for calculating statistics collected from Riot API
 */
public interface IStatsService {
    /**
     * Collects data from riot api about last X matches of the specified player
     * Calculate stats from these data, including number of games played, number of wins and win rate, and also whether player is on winning
     * or losing streak
     *
     * @param server tag of the wanted server
     * @param userId account id of the wanted player
     * @param timestamp date and time of the match played by the given player
     * @param summonerName username of the wanted player
     * @param team on which teaam the player played
     * @return match stats from X last games played of specified user
     */
    StatsBean getStatsForPlayer(String server, String userId, long timestamp, String summonerName, int team);
}
