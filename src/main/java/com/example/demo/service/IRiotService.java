package com.example.demo.service;

import com.example.demo.model.MatchDetailBean;
import com.example.demo.model.MatchesBean;
import org.springframework.stereotype.Service;

@Service
/**
 * Interface for work with Riot API
 */
public interface IRiotService {
    /**
     * Get account id of the specified user
     *
     * @param server tag of the wanted server
     * @param username username of the wanted player
     * @return account id of the given user
     */
    String getAcccountId(String server, String username);

    /**
     * Get match history of last 20 matches of the given player
     *
     * @param server tag of the wanted server
     * @param id account id of the wanted player
     * @return match history
     */
    MatchesBean getMatchHistory(String server, String id);

    /**
     * Get match history of last 'matches' games of the given player
     *
     * @param server tag of the wanted server
     * @param id account id of the wanted player
     * @param matches number of matches to consider
     * @return match history
     */
    MatchesBean getMatchHistory(String server, String id, int matches);

    /**
     * Get match history of last 'matches' games of the given player played before 'timestamp'
     *
     * @param server tag of the wanted server
     * @param id account id of the wanted player
     * @param matches number of matches to consider
     * @param timestamp date and time of the match played by the given player
     * @return match history
     */
    MatchesBean getMatchHistory(String server, String id, int matches, long timestamp);

    /**
     * Get detailed statistics of the given match
     *
     * @param server tag of the wanted server
     * @param id game id of the specified game
     * @return statistics of the given match
     */
    MatchDetailBean getMatchDetail(String server, String id);
}
