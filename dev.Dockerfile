FROM openjdk:13-alpine
VOLUME /tmp
ADD /target/*.jar lol-statistics-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=dev" , "/lol-statistics-0.0.1-SNAPSHOT.jar"]